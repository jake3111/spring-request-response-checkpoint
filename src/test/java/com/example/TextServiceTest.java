package com.example;

import com.example.Service.TextService;
import org.junit.Test;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextServiceTest {

    private TextService textService = new TextService();

    @Test
    public void camelize() {
        textService = new TextService();
        Map<String, String> requestParameters = new HashMap<>();
        requestParameters.put("original", "this_should_be_in_camel_case");
        assertEquals("thisShouldBeInCamelCase", textService.camelize(requestParameters));
    }

    @Test
    public void camelizeWithCaps() {
        textService = new TextService();
        Map<String, String> requestParameters = new HashMap<>();
        requestParameters.put("original", "this_should_be_in_camel_case");
        requestParameters.put("initialCap", "true");
        assertEquals("ThisShouldBeInCamelCase", textService.camelize(requestParameters));
    }

    @Test
    public void redactOnSampleString() {
        textService = new TextService();
        Set<String> badWordsSet = new HashSet();
        badWordsSet.add("little");
        badWordsSet.add("this");
        String original = "A little of this and a little of that";
        assertEquals("A ****** of **** and a ****** of that", textService.redact(badWordsSet, original));
    }

    @Test
    public void encodeOnSampleString() {
        textService = new TextService();
        String message = "a little of this and a little of that";
        String key = "mcxrstunopqabyzdefghijklvw";
        String result = textService.encode(message, key);
        assertEquals("m aohhas zt hnog myr m aohhas zt hnmh", result);
    }

    @Test
    public void sedOnSampleString() {
        textService = new TextService();
        String message = "a little of this and a little of that";
        Map <String, String> pathVariables = new HashMap<>();
        pathVariables.put("find", "little");
        pathVariables.put("replacement", "lot");
        String result = textService.sed(pathVariables, message);
        assertEquals("a lot of this and a lot of that", result);
    }

    @Test
    public void sedOnSampleStringWithSpaces() {
        textService = new TextService();
        String message = "Heat Blast and Cold Fusion";
        Map <String, String> pathVariables = new HashMap<>();
        pathVariables.put("find", "Heat Blast");
        pathVariables.put("replacement", "Cold Fusion");
        String result = textService.sed(pathVariables, message);
        assertEquals("Cold Fusion and Cold Fusion", result);
    }
}
