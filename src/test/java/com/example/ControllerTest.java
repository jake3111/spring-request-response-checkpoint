package com.example;

import com.example.Controller.TextController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TextController.class)
class ControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testCamelizeNoInitialCap() throws Exception{
        RequestBuilder request = MockMvcRequestBuilders.get("/camelize?original=this_is_a_thing");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("thisIsAThing"));
    }

    @Test
    void testCamelizeWithInitialCap() throws Exception{
        RequestBuilder request = MockMvcRequestBuilders.get("/camelize?original=this_is_a_thing&initialCap=true");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("ThisIsAThing"));
    }

    @Test
    void testRedact() throws Exception{
        RequestBuilder request = MockMvcRequestBuilders.get("/redact?original=A little of this and a lot of that&badWord=little&badWord=this&badWord=lot");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("A ****** of **** and a *** of that"));
    }

    @Test
    void testEncode() throws Exception{
        RequestBuilder request = MockMvcRequestBuilders.post("/encode?message=abcdefghijklmnopqrstuvwxyz&key=mcxrstunopqabyzdefghijklvw");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("mcxrstunopqabyzdefghijklvw"));
    }

    @Test
    void testSed() throws Exception{
        RequestBuilder request = MockMvcRequestBuilders.post("/s/Sam/Max")
                .content("Max met up with Sam at the house of Sam");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("Max met up with Max at the house of Max"));
    }
}
