package com.example.Service;

import java.util.*;

public class TextService {

    public String camelize(Map requestParameters){
        StringBuilder camelCaseStringBuilder = new StringBuilder();
        String[] snakeCaseStringArray = ((String)requestParameters.get("original")).split("_");
        if (requestParameters.get("initialCap") != null && requestParameters.get("initialCap").equals("true")){
            camelCaseStringBuilder.append(snakeCaseStringArray[0].substring(0, 1).toUpperCase());
            camelCaseStringBuilder.append(snakeCaseStringArray[0].substring(1));
        } else {
            camelCaseStringBuilder.append(snakeCaseStringArray[0]);
        }
        for (int i = 1; i < snakeCaseStringArray.length;i++) {
            camelCaseStringBuilder.append(snakeCaseStringArray[i].substring(0, 1).toUpperCase());
            camelCaseStringBuilder.append(snakeCaseStringArray[i].substring(1));
        }
        return camelCaseStringBuilder.toString();
    }

    public String redact(Set<String> badWords, String orginal){
        String [] originalArray = orginal.split(" ");
        StringBuilder returnStringBuilder = new StringBuilder();
        for (String localString: originalArray) {
            if (badWords.contains(localString)) {
                for (int i = 0; i < localString.length(); i++) {
                    returnStringBuilder.append("*");
                }
            } else{
                returnStringBuilder.append(localString);
            }
            returnStringBuilder.append(" ");
        }
        return returnStringBuilder.toString().trim();
    }

    public String encode(String message, String key){
        //creating a map for the alphabet to the string
        Map<Character, Character> keyConversion = new HashMap<>();
        char alphabetCharacter = 'a';
        for (int i = 0;i < key.length();i++) {
            keyConversion.put(alphabetCharacter, key.charAt(i));
            alphabetCharacter++;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < message.length(); i++){
            if (keyConversion.containsKey(message.charAt(i))){
                sb.append(keyConversion.get(message.charAt(i)));
            } else {
                sb.append(message.charAt(i));
            }
        }
        return sb.toString();
    }

    public String sed(Map pathVariables, String message){
        String findString = (String)pathVariables.get("find");
        String[] findArray = (findString).split(" ");
        String replace = (String)pathVariables.get("replacement");
        StringBuilder sb = new StringBuilder();

        String [] messageArray = message.split(" ");
        int i;
        for (i = 0;i < messageArray.length - (findArray.length - 1);i++){
            StringBuilder localStringBuilder = new StringBuilder();
            for (int k = 0;k < findArray.length; k ++){
                localStringBuilder.append(messageArray[i + k]);
                if (k < (findArray.length - 1)){
                    localStringBuilder.append(" ");
                }
            }
            if (localStringBuilder.toString().equals(findString)){
                sb.append(replace);
                i += (findArray.length - 1);
            } else {
                sb.append(messageArray[i]);
            }
            sb.append(" ");
        }
        for (i = i; i < messageArray.length;i++) {
            sb.append(messageArray[i]);
            sb.append(" ");
        }
        return sb.toString().trim();
    }
}
