package com.example.Controller;

import com.example.Service.TextService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
public class TextController {
    private TextService textService = new TextService();


    @GetMapping("/")
    public String helloWorld() {
        return "Hello from Spring!";
    }

    @GetMapping("/camelize")
    public String camelize(@RequestParam Map requestParameters) {
        return textService.camelize(requestParameters);
    }

    @GetMapping("/redact")
    public String redact(@RequestParam(value = "badWord") Set badWordSet, @RequestParam(value = "original") String original) {
        return textService.redact(badWordSet, original);
    }

    @PostMapping("/encode")
    public String redact(@RequestParam(value = "message") String message, @RequestParam(value = "key") String key) {
        return textService.encode(message, key);
    }

    @PostMapping("/s/{find}/{replacement}")
    public String sed(@PathVariable Map pathVariables, @RequestBody String document) {
        return textService.sed(pathVariables, document);
    }
}
